package tests

import (
	"context"
	"encoding/json"
	"github.com/kelseyhightower/envconfig"
	"github.com/nats-io/stan.go"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/wiretap-microservice/adapters"
	"gitlab.com/cyverse/wiretap-microservice/domain"
	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
	"os"
	"strings"
	"testing"
	"time"
)

// This is an integration test that require a nats-streaming instance.
// CI_INTEGRATION_STAN, NATS_URL, NATS_CLUSTER_ID environment variable needs to be properly set for this test to run.
//
// - create an instance of Domain and start it.
// - send a bunch of cloudevents via STAN.
// - check if the log file has all the events properly logged.
func Test_Wiretap_STAN(t *testing.T) {
	if !isIntegrationTest() {
		t.Skip("Not integration test, skipping...")
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	config := prepareConfig(t)

	const eventCount = 1000

	go entrypoint(ctx, cancel, config)
	time.Sleep(time.Second / 2)
	sc := newStanConn(config)
	for i := 0; i < eventCount; i++ {
		sendCloudEvent(sc)
	}

	time.Sleep(time.Second * 2)
	checkLogFile(t, config.FileStoreDir, eventCount)
}

func prepareConfig(t *testing.T) utils.Config {
	tempDir := t.TempDir() // use tmp directory for log files
	t.Logf("temp dir for logs: %s", tempDir)

	var config utils.Config
	err := envconfig.Process("", &config)
	if err != nil {
		t.Fatal(err.Error())
	}
	config.NatsClientID = "integration-test"
	config.FileStoreDir = tempDir
	config.ProcessDefaults()
	return config
}

func entrypoint(ctx context.Context, cancel context.CancelFunc, config utils.Config) {
	config.NatsClientID = "wiretap" // use a separate nats client ID for the service code

	// create an initial Domain object
	var dmain domain.Domain

	// add and initialize the query adapter
	dmain.QueryIn = &adapters.QueryAdapter{}
	dmain.QueryIn.Init(config)
	defer dmain.QueryIn.Close()

	// add and initialize the events adapter
	dmain.EventsIn = &adapters.StanEventAdapter{}
	dmain.EventsIn.Init(config)
	defer dmain.EventsIn.Close()

	dmain.MessageStore = &adapters.WiretapFileStore{}
	dmain.MessageStore.Init(config)

	dmain.Start(ctx, cancel)
}

func newStanConn(config utils.Config) stan.Conn {
	sc, err := stan.Connect(config.NatsClusterID, config.NatsClientID, stan.NatsURL(config.NatsURL))
	if err != nil {
		log.WithError(err).Fatal("cannot connect to stan")
	}
	return sc
}

func sendCloudEvent(sc stan.Conn) {
	ce := utils.NewCloudEventDataString(`{"foo":"bar"}`)
	ceMarshal, err := ce.MarshalJSON()
	if err != nil {
		log.WithError(err).Fatal("fail to marshal cloudevent")
	}
	err = sc.Publish("cyverse.events", ceMarshal)
	if err != nil {
		log.WithError(err).Fatal("fail to publish stan events")
	}
	log.Info("published test ce")
}

func checkLogFile(t *testing.T, baseDir string, expectedEventCount int) {
	file, err := os.ReadFile(currenLogFilename(baseDir))
	if !assert.NoError(t, err) {
		return
	}
	lines := strings.Split(string(file), "\n") // split by '\n' will emit an extra empty line at the end
	nonEmptyLineCount := 0
	for _, line := range lines {
		if len(line) == 0 {
			continue
		}
		nonEmptyLineCount++
		var data types.TracedMessage
		err = json.Unmarshal([]byte(line), &data)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, "cyverse.events", data.Subject)
		assert.Equal(t, "cyverse.mock", data.CloudEventSource)
		assert.Equal(t, "string", data.CloudEventType)
		assert.Equal(t, json.RawMessage(`{"foo":"bar"}`), data.Payload)
		_, err = xid.FromString(data.CloudEventID)
		assert.NoError(t, err)
	}
	assert.Equal(t, expectedEventCount, nonEmptyLineCount)
}

func isIntegrationTest() bool {
	envVal, ok := os.LookupEnv("CI_INTEGRATION_STAN")
	if !ok {
		return false
	}
	return envVal == "true"
}

func currenLogFilename(baseDir string) string {
	now := time.Now().UTC()
	return adapters.LogFileName(baseDir, "wiretap-", now)
}
