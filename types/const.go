package types

import "time"

// TODO: DEVELOPER should changes these e.g. instead of "mythical*", change to "user*" for user microservice, "credentials*", "workspaces*", etc
const (
	DefaultNatsURL           = "nats://nats:4222"
	DefaultNatsMaxReconnect  = 6                // max times to reconnect within nats.connect()
	DefaultNatsReconnectWait = 10 * time.Second // seconds to wait within nats.connect()

	DefaultNatsClientID      = "wiretap"
	DefaultNatsQGroup        = "wiretap_qgroup"
	DefaultNatsClusterID     = "cacao-cluster"
	DefaultNatsDurableName   = "wiretap_durable"
	DefaultNatsEventsSubject = "cyverse.events"

	DefaultNatsCoreSubject = ">"                        // this listens to all
	DefaultNatsCoreTimeout = 24 * 60 * 60 * time.Second // timeout before wiretap exipires, currently set so that 1 day is a-ok
	//DefaultNatsCoreTimeout = 30 * time.Second // timeout before wiretap exipires, currently set so that 1 day is a-ok
)

// These constants are for channels
const (
	DefaultChannelBufferSize = 100
)

// These constants are for query operations to be used within the microservice
// To use these effectively, make them exactly the same as the CACAO subjects and then
// they can be reused for both the appropriate Domain and the Adapters
// TODO: TEMPLATE, we should consolidate these query "ops" in a cacao common submodule
const (
	WiretapCatchallQueryOp QueryOp = "cyverse.*" // some microservices may want to catchall vs specific ops
)

// These constants are for events to be used within the microservice
// To use these affective, make them exactly the same as CACAO events and then
// they can be reused for both the appropriate Domain and Adapters
// TODO: TEMPLATE, we should consider consolidating these events in a cacao common submodule
const (
	WiretapCatchallEvent EventType = "*" // some microservices may want a catchall vs specific events
)
