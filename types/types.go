package types

import (
	"encoding/json"

	cloudevents "github.com/cloudevents/sdk-go/v2"
)

// This file contains types which are used across the entire microservice, which may not be more appropriate elsewhere

// WiretapData is the primary data type when receiving and sending data externally, and possibly destined to a persisent store (if applicable)
// As a implementor you should change this struct to meet the needs (e.g. a user ms would have username, first name, last name, email, etc)
type WiretapData struct {
	Subject string
	Reply   string
	Data    cloudevents.Event

	Event     string // may be empty or nil if not a event
	Sequence  uint64 // may be 0 or nil if not an event
	Timestamp int64  // may be 0 or nil if not an event
}

// QueryOp is a type to represent query operations
type QueryOp string

// EventType is a type to represent event operations
type EventType string

// TracedMessage is the struct used to dump data from the wire
type TracedMessage struct {
	TimeStamp        string          `json:"timestamp"`
	Actor            string          `json:"actor"`
	Emulator         string          `json:"emulator"`
	TransactionID    string          `json:"tid"`
	Subject          string          `json:"subject"`
	CloudEventID     string          `json:"cloud_event_id"`
	CloudEventSource string          `json:"cloud_event_source"`
	CloudEventType   string          `json:"cloud_event_type"`
	Payload          json.RawMessage `json:"payload"`
}
