package main

import (
	"context"
	"os"
	"os/signal"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/cyverse/wiretap-microservice/adapters"
	"gitlab.com/cyverse/wiretap-microservice/domain"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

func main() {

	// TODO: make this configurable
	log.SetReportCaller(false)

	log.Debug("main: setting up cancelable context")
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var config utils.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.Fatal(err.Error())
	}
	config.ProcessDefaults()

	log.Info("nats url =", config.NatsURL)

	// create an initial Domain object
	var dmain domain.Domain

	// add and initialize the query adapter
	dmain.QueryIn = &adapters.QueryAdapter{}
	dmain.QueryIn.Init(config)
	defer dmain.QueryIn.Close()

	// add and initialize the events adapter
	dmain.EventsIn = &adapters.StanEventAdapter{}
	dmain.EventsIn.Init(config)
	defer dmain.EventsIn.Close()

	dmain.MessageStore = &adapters.WiretapFileStore{}
	dmain.MessageStore.Init(config)
	defer dmain.MessageStore.Close()

	cancelCtxWhenInterrupted(cancel)

	dmain.Start(ctx, cancel)
}

// cancel the main context when receiving an interrupt (Ctrl-C) signal
func cancelCtxWhenInterrupted(cancel context.CancelFunc) {
	interruptSignalChan := make(chan os.Signal, 1)
	signal.Notify(interruptSignalChan, os.Interrupt)
	go func() {
		<-interruptSignalChan
		log.Info("interrupt signal received, cancel context")
		cancel()

		// give the process 5 seconds to shut down gracefully, otherwise force shutdown with os.Exit()
		time.Sleep(time.Second * 5)
		log.Warn("5 seconds after receiving interrupt signal, force shutdown")
		os.Exit(1)
	}()
}
