FROM golang:1.23 as build
ARG SKAFFOLD_GO_GCFLAGS
COPY ./ /wiretap-microservice/
WORKDIR /wiretap-microservice/
RUN eval go build -gcflags="${SKAFFOLD_GO_GCFLAGS}"

FROM gcr.io/distroless/base-debian12:nonroot
ARG GOTRACEBACK
ENV GOTRACEBACK="$GOTRACEBACK"
COPY --from=build /wiretap-microservice/wiretap-microservice /
CMD ["/wiretap-microservice"]
