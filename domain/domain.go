package domain

import (
	"context"
	log "github.com/sirupsen/logrus"
	"sync"

	"gitlab.com/cyverse/wiretap-microservice/ports"
	"gitlab.com/cyverse/wiretap-microservice/types"
)

// Domain is the base struct for the domain service
type Domain struct {
	QueryIn      ports.IncomingQueryPort
	EventsIn     ports.IncomingEventPort
	MessageStore ports.PersistentStoragePort
}

// Start will start the domain object, and in turn start all the async adapters
func (d Domain) Start(ctx context.Context, cancel context.CancelFunc) {
	logger := log.WithFields(log.Fields{
		"function": "domain.Domain.processWorker",
	})
	logger.Debug("called")

	var wg sync.WaitGroup

	dc := make(chan types.WiretapData, types.DefaultChannelBufferSize)

	logger.Trace("starting QueryIn.Start")
	d.QueryIn.InitChannel(dc)
	wg.Add(1)
	go d.QueryIn.Start(ctx, &wg)

	logger.Trace("starting EventsIn.Start")
	d.EventsIn.InitChannel(dc)
	wg.Add(1)
	go d.EventsIn.Start(ctx, &wg)

	// start the domain's query worker
	wg.Add(1)
	logger.Trace("starting processWorker")
	go d.processWorker(ctx, dc, &wg, cancel)

	// wait for query adapter, event adapter, processWorker to stop gracefully
	wg.Wait()
}

func (d Domain) processWorker(ctx context.Context, dc <-chan types.WiretapData, wg *sync.WaitGroup, cancel context.CancelFunc) {
	logger := log.WithFields(log.Fields{
		"function": "domain.Domain.processWorker",
	})
	logger.Debug("called")
	defer wg.Done()

	for {
		select {
		case data := <-dc:
			logger.Trace("received new data")
			err := d.MessageStore.Create(data)
			if err != nil {
				logger.WithError(err).Error("fail to store msg")
				cancel() // if there is error from storage, then cancel the context for a graceful shutdown
			}
		case <-ctx.Done():
			logger.Info("context cancellation")
			d.drainChannel(logger, dc)
			return
		}
	}
}

// drain the channel and write all msg to storage.
func (d Domain) drainChannel(logger *log.Entry, dc <-chan types.WiretapData) {
	for len(dc) > 0 {
		data := <-dc
		err := d.MessageStore.Create(data)
		if err != nil {
			logger.WithError(err).Error("fail to store msg")
		}
	}
}
