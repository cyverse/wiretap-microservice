package adapters

import (
	"context"
	"github.com/nats-io/nats-server/v2/server"
	"github.com/stretchr/testify/assert"
	"strconv"
	"sync"
	"testing"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats.go"
	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

const (
	NatsQueryTestPort = 31313
)

func TestQueryAdapter_Init(t *testing.T) {
	type fields struct {
		config        utils.Config
		domainChannel chan types.WiretapData
		waitgroup     *sync.WaitGroup
	}
	type args struct {
		c utils.Config
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "default",
			fields: fields{
				config:        utils.Config{},
				domainChannel: make(chan types.WiretapData),
				waitgroup:     &sync.WaitGroup{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := &QueryAdapter{
				config:        tt.fields.config,
				domainChannel: tt.fields.domainChannel,
				wg:            tt.fields.waitgroup,
			}
			q.Init(tt.args.c)
		})
	}
}

func TestQueryAdapter_Start(t *testing.T) {
	type fields struct {
		config        utils.Config
		domainChannel chan types.WiretapData
		waitgroup     *sync.WaitGroup
		doCtxCancel   bool
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			name: "simple message",
			fields: fields{
				config: utils.Config{
					NatsURL:      "nats://localhost:" + strconv.Itoa(NatsQueryTestPort),
					NatsClientID: types.DefaultNatsClientID,
					NatsQgroup:   types.DefaultNatsQGroup,
				},
				domainChannel: make(chan types.WiretapData, 1),
				waitgroup:     &sync.WaitGroup{},
				doCtxCancel:   false,
			},
		},
		{
			name: "context cancel",
			fields: fields{
				config: utils.Config{
					NatsURL:      "nats://localhost:" + strconv.Itoa(NatsQueryTestPort),
					NatsClientID: types.DefaultNatsClientID,
					NatsQgroup:   types.DefaultNatsQGroup,
				},
				domainChannel: make(chan types.WiretapData),
				waitgroup:     &sync.WaitGroup{},
				doCtxCancel:   true,
			},
		},
	}
	for _, tt := range tests {

		t.Run(tt.name, func(t *testing.T) {

			// create a new context
			ctx, cancel := context.WithCancel(context.Background())

			t.Log("starting test nats server")
			server := utils.RunNatsTestServer(NatsQueryTestPort)
			defer server.Shutdown()

			q := &QueryAdapter{
				config:        tt.fields.config,
				domainChannel: tt.fields.domainChannel,
				//wg:            tt.fields.waitgroup,
			}

			// increment the waitgroup
			tt.fields.waitgroup.Add(1)

			t.Log("go q.start()")
			go q.Start(ctx, tt.fields.waitgroup)

			// sleep
			t.Log("sleeping")
			time.Sleep(2 * time.Second)

			// check if we should test cancel
			if !tt.fields.doCtxCancel {

				t.Log("send cloud event")
				nc, _ := nats.Connect(tt.fields.config.NatsURL)

				event := cloudevents.NewEvent()
				event.SetID("xxx")
				event.SetType("test")
				event.SetTime(time.Now())
				event.SetSource("TestQueryAdapter_Start")
				event.SetDataContentType("application/json;charset=utf-8")
				event.SetData(cloudevents.TextPlain, "test message")
				j, _ := event.MarshalJSON()
				nc.Publish("cyverse.test", j)
				nc.Flush()
				nc.Close()
				t.Log("done sending cloud event")
			}

			// close the domain channel and then wait
			cancel()
			t.Log("waiting for adapter to close")
			tt.fields.waitgroup.Wait()
			t.Log("closing domainChannel")
			close(tt.fields.domainChannel)
			server.Shutdown()
		})
	}
}

func TestQueryAdapter_Start_context_cancel(t *testing.T) {
	//t.Skip()
	//return
	config := utils.Config{
		NatsURL:      "nats://localhost:" + strconv.Itoa(NatsQueryTestPort),
		NatsClientID: types.DefaultNatsClientID,
		NatsQgroup:   types.DefaultNatsQGroup,
	}
	type testSetup struct {
		ctx           context.Context
		cancel        context.CancelFunc
		q             *QueryAdapter
		wg            *sync.WaitGroup
		natsServer    *server.Server
		domainChannel chan types.WiretapData
	}

	newTestSetup := func(channelBuffer uint) testSetup {
		// create a new context
		ctx, cancel := context.WithCancel(context.Background())

		t.Log("starting test nats server")
		natsServer := utils.RunNatsTestServer(NatsQueryTestPort)

		domainChannel := make(chan types.WiretapData, channelBuffer)
		var wg sync.WaitGroup
		q := &QueryAdapter{}
		q.Init(config)
		q.InitChannel(domainChannel)
		wg.Add(1)
		return testSetup{
			ctx:           ctx,
			cancel:        cancel,
			q:             q,
			wg:            &wg,
			natsServer:    natsServer,
			domainChannel: domainChannel,
		}
	}

	t.Run("cancel immediately no query sent", func(t *testing.T) {
		setup := newTestSetup(0)
		defer setup.natsServer.Shutdown()

		t.Log("go q.start()")
		go setup.q.Start(setup.ctx, setup.wg)

		setup.cancel()
		waitForAdapterToExit(t, setup.wg, setup.domainChannel)
	})
	t.Run("cancel w/ one query sent", func(t *testing.T) {
		setup := newTestSetup(1)
		defer setup.natsServer.Shutdown()

		t.Log("go q.start()")
		go setup.q.Start(setup.ctx, setup.wg)

		// sleep to wait for query adapter to connect to NATS
		t.Log("sleeping")
		time.Sleep(2 * time.Second)

		t.Log("send cloud event")
		connectAndSendNatsMsgs(config, 1)
		t.Log("done sending cloud event")

		setup.cancel()
		waitForAdapterToExit(t, setup.wg, setup.domainChannel)
	})
	t.Run("cancel w/ multiple queries sent", func(t *testing.T) {
		setup := newTestSetup(1000)
		defer setup.natsServer.Shutdown()

		t.Log("go q.start()")
		go setup.q.Start(setup.ctx, setup.wg)

		// no wait for adapter to connect, start sending msg right away

		t.Log("send cloud event")
		stop := make(chan struct{})
		done := make(chan struct{})
		go connectAndSendNatsMsgUntilStopped(config, stop, done)
		t.Log("done sending cloud event")

		time.Sleep(time.Second)

		setup.cancel()
		waitForAdapterToExit(t, setup.wg, setup.domainChannel)
		stop <- struct{}{} // stop sending test msg
		<-done

		assert.Greaterf(t, len(setup.domainChannel), 0, "even without waiting, we should receive some events in the channel")
	})
}

// cancel the context, and wait for adapter to exit
func waitForAdapterToExit(t *testing.T, wg *sync.WaitGroup, domainChannel chan types.WiretapData) {
	t.Log("waiting for adapter to close")
	wg.Wait()
	t.Log("closing domainChannel")
	// no one should be writing to the domain channel after wait group finishes waiting,
	// close the domainChannel to check this (any write to closed domainChannel will panic).
	close(domainChannel)
}

func connectAndSendNatsMsgs(config utils.Config, msgCount int) {
	nc, err := nats.Connect(config.NatsURL)
	if err != nil {
		panic(err.Error())
	}
	defer nc.Close()
	for i := 0; i < msgCount; i++ {
		sendNatsMsg(nc)
	}
	err = nc.Flush()
	if err != nil {
		panic(err.Error())
	}
}

// connect to NATS, keep send test msg until receiving from stop channel. write to done channel before return.
func connectAndSendNatsMsgUntilStopped(config utils.Config, stop <-chan struct{}, done chan<- struct{}) {
	defer func() { done <- struct{}{} }()

	nc, err := nats.Connect(config.NatsURL)
	if err != nil {
		panic(err.Error())
	}
	defer nc.Close()
	for {
		var stopped bool
		select {
		case <-stop:
			stopped = true
		default:
			stopped = false
		}
		if stopped {
			break
		}
		sendNatsMsg(nc)
	}
	err = nc.Flush()
	if err != nil {
		panic(err.Error())
	}
}

func sendNatsMsg(nc *nats.Conn) {
	event := cloudevents.NewEvent()
	event.SetID("xxx")
	event.SetType("test")
	event.SetTime(time.Now())
	event.SetSource("TestQueryAdapter_Start")
	event.SetDataContentType("application/json;charset=utf-8")
	_ = event.SetData(cloudevents.TextPlain, "test message")
	j, _ := event.MarshalJSON()
	err := nc.Publish("cyverse.test", j)
	if err != nil {
		panic(err.Error())
	}
}

func TestQueryAdapter_InitChannel(t *testing.T) {
	q := &QueryAdapter{}
	q.InitChannel(make(chan types.WiretapData))
	assert.NotNil(t, q.domainChannel)
}
