package adapters

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"path"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

// WiretapFileStore is file storage for wiretap data.
type WiretapFileStore struct {

	// currentDate stores a string of the current date (in UTC)
	currentDate string

	// logDir directory to store new logs
	logDir string

	// prefix of the log file
	prefix string

	// file handle to current log
	currentFile *os.File

	// writer
	currentWriter bufio.Writer
}

// Init is empty initialization for WiretapFileStore
func (wfs *WiretapFileStore) Init(c utils.Config) {
	logger := log.WithFields(log.Fields{
		"function": "adapters.WiretapFileStore.Init",
	})
	logger.Debug("called")

	// verify or create the log directory
	err := os.MkdirAll(c.FileStoreDir, 0775)
	if err != nil {
		logger.WithField("logDir", c.FileStoreDir).Fatalln("could not create or modify the log directory")
	}

	wfs.logDir = path.Clean(c.FileStoreDir)
	wfs.prefix = c.FileStoreLogPrefix

	wfs.checkOrRotateFileLog()
}

func (wfs *WiretapFileStore) checkOrRotateFileLog() {
	logger := log.WithFields(log.Fields{
		"function":    "adapters.WiretapFileStore.checkOrRotateFileLog",
		"currentDate": wfs.currentDate,
	})
	logger.Trace("called")

	// get the current date string, in UTC
	currentDate := dateString(time.Now().UTC())

	// if wfs.currentDate is different, rotate log!
	if wfs.currentDate != currentDate {

		newLogFileName := logFileName(wfs.logDir, wfs.prefix, currentDate)
		logger.Debug("creating/appending log, " + newLogFileName)

		// first close the old handle
		// TODO: what is the best way to finalize this without  setFinalizer
		wfs.flushAndCloseCurrentLogFile(logger)

		// create a new file, this is fatal if error
		newLogFile, err := os.OpenFile(newLogFileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0664)
		if err != nil {
			logger.WithError(err).Fatal("fail to open new log file")
		}

		wfs.currentDate = currentDate
		wfs.currentFile = newLogFile
		wfs.currentWriter = *bufio.NewWriter(newLogFile)
	}
}

func (wfs *WiretapFileStore) flushAndCloseCurrentLogFile(logger *log.Entry) {
	if wfs.currentFile == nil {
		return
	}
	err := wfs.currentWriter.Flush()
	if err != nil {
		logger.WithField("currentFilename", wfs.currentFile.Name()).Warningln("fail to flush writer")
	}
	err = wfs.currentFile.Close()
	if err != nil {
		logger.WithField("currentFilename", wfs.currentFile.Name()).Warningln("could not close file on rotate")
	}
}

// Create outputs the message into file.
// Currently, this method always return nil (no error). Any errors occurred inside this method are logged and ignored.
// Note this method is NOT thread-safe.
func (wfs *WiretapFileStore) Create(data types.WiretapData) error {
	logger := log.WithFields(log.Fields{
		"function": "adapters.WiretapFileStore.Create",
	})
	logger.Trace("called")

	logEvent(log.StandardLogger(), data)

	tm, err := wiretapDataToTracedMessage(data)
	if err != nil {
		logger.WithError(err).Warn("fail to convert wiretap data to trace msg")
		// log and ignore error for now
		return nil
	}

	logger.Tracef("%+v\n", tm)
	wfs.checkOrRotateFileLog()
	if err = json.NewEncoder(&wfs.currentWriter).Encode(tm); err != nil {
		logger.WithError(err).Error("fail to write json to writer")
	}
	if err = wfs.currentWriter.Flush(); err != nil {
		logger.WithError(err).Error("fail to flush writer")
	}
	return nil
}

// Close flushes and closes the current log file
func (wfs *WiretapFileStore) Close() {
	logger := log.WithFields(log.Fields{
		"function":    "adapters.WiretapFileStore.Close",
		"currentDate": wfs.currentDate,
	})
	wfs.flushAndCloseCurrentLogFile(logger)
}

func wiretapDataToTracedMessage(data types.WiretapData) (types.TracedMessage, error) {
	b := json.RawMessage(data.Data.Data())
	if b == nil {
		// for now, don't print error, just print nil
		return types.TracedMessage{}, fmt.Errorf("data is nil, CloudEvent data could not be retrieved")
	}

	var r map[string]interface{}
	err := json.Unmarshal(b, &r)
	if err != nil {
		return types.TracedMessage{}, fmt.Errorf("could not unmarshal cloud event data, %w", err)
	}

	tm := types.TracedMessage{
		TimeStamp:        data.Data.Context.GetTime().String(),
		Actor:            "",
		Emulator:         "",
		TransactionID:    "",
		Subject:          data.Subject,
		CloudEventID:     data.Data.Context.GetID(),
		CloudEventSource: data.Data.Context.GetSource(),
		CloudEventType:   data.Data.Context.GetType(),
		Payload:          b,
	}
	if actor, ok := extractStringFieldFromMap(r, "actor"); ok {
		tm.Actor = actor
	}
	if emulator, ok := extractStringFieldFromMap(r, "emulator"); ok {
		tm.Emulator = emulator
	}
	tidRaw, err := data.Data.Context.GetExtension("TransactionID")
	if err == nil {
		if tid, ok := tidRaw.(string); ok {
			tm.TransactionID = tid
		}
	}
	return tm, nil
}

func extractStringFieldFromMap(m map[string]interface{}, fieldName string) (result string, ok bool) {
	if fieldValueRaw, ok := m[fieldName]; ok {
		fieldValueString, ok := fieldValueRaw.(string)
		if ok {
			return fieldValueString, true
		}
	}
	return "", false
}

func logEvent(logger *log.Logger, data types.WiretapData) {
	mediaType, _ := data.Data.Context.GetDataMediaType()
	logger.Printf("Message: nats subject=%s nats reply=%s, cloud event metadata={id=%s, source=%s, specversion=%s, type=%s, datacontenttype=%s, dataschema=%s, subject=%s, data-media-type=%s, time=%s}\n",
		data.Subject, data.Reply, data.Data.Context.GetID(), data.Data.Context.GetSource(),
		data.Data.Context.GetSpecVersion(), data.Data.Context.GetType(), data.Data.Context.GetDataContentType(),
		data.Data.Context.GetDataSchema(), data.Data.Context.GetSubject(),
		mediaType, data.Data.Context.GetTime())
}

func dateString(timestamp time.Time) string {
	return timestamp.Format("2006-01-02")
}

func logFileName(logDir, fileNamePrefix, dateStr string) string {
	return path.Join(logDir, fileNamePrefix+dateStr+".log")
}

// LogFileName returns the filename for current log file
func LogFileName(logDir, fileNamePrefix string, currTime time.Time) string {
	currentDate := currTime.Format("2006-01-02")
	return path.Join(logDir, fileNamePrefix+currentDate+".log")
}
