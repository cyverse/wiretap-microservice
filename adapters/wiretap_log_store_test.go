package adapters

import (
	"testing"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

func TestWiretapLogStore_Init(t *testing.T) {
	type args struct {
		c utils.Config
	}
	tests := []struct {
		name string
		wfs  *WiretapLogStore
		args args
	}{
		{
			name: "default",
			wfs:  &WiretapLogStore{},
			args: args{c: utils.Config{}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			wfs := &WiretapLogStore{}
			wfs.Init(tt.args.c)
		})
	}
}

func TestWiretapLogStore_Create(t *testing.T) {
	type args struct {
		data types.WiretapData
	}
	tests := []struct {
		name    string
		wfs     *WiretapLogStore
		args    args
		wantErr bool
	}{
		{
			name: "empty cloud event data test",
			args: args{data: types.WiretapData{Subject: "subject",
				Reply: "reply", Data: cloudevents.NewEvent()}},
			wantErr: false,
		},
		{
			name: "cloud event data test 1: string data",
			args: args{data: types.WiretapData{Subject: "subject",
				Reply: "reply", Data: utils.NewCloudEventDataString("test 1")}},
			wantErr: false,
		},
		{
			name: "cloud event data test 2: json data",
			args: args{data: types.WiretapData{Subject: "subject",
				Reply: "reply", Data: utils.NewCloudEventDataJSON([]byte("{data:'test 2'}"))}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			wfs := &WiretapLogStore{}
			if err := wfs.Create(tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("WiretapLogStore.Create() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
