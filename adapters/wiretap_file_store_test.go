package adapters

import (
	"encoding/json"
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/wiretap-microservice/types"
	"testing"
	"time"
)

func Test_wiretapDataToTracedMessage(t *testing.T) {
	type args struct {
		data types.WiretapData
	}
	tests := []struct {
		name    string
		args    args
		want    types.TracedMessage
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "query",
			args: args{
				data: types.WiretapData{
					Subject: "test-subject-123",
					Reply:   "test-reply-123",
					Data: func() cloudevents.Event {
						ce := cloudevents.NewEvent()
						_ = ce.SetData(cloudevents.TextPlain, `
{
"actor": "actor123",
"emulator": "emulator123"
}`)
						ce.SetTime(time.Date(2022, 12, 22, 0, 0, 0, 0, time.UTC))
						ce.SetID("aaaaaaaaaaaaaaaaaaaa")
						ce.SetSource("ce-src-123")
						ce.SetType("ce-type-123")
						ce.SetExtension("TransactionID", "tid-aaaaaaaaaaaaaaaaaaaa")
						return ce
					}(),
					Event:     "",
					Sequence:  0,
					Timestamp: 0,
				},
			},
			want: types.TracedMessage{
				TimeStamp:        time.Date(2022, 12, 22, 0, 0, 0, 0, time.UTC).String(),
				Actor:            "actor123",
				Emulator:         "emulator123",
				TransactionID:    "tid-aaaaaaaaaaaaaaaaaaaa",
				Subject:          "test-subject-123",
				CloudEventID:     "aaaaaaaaaaaaaaaaaaaa",
				CloudEventSource: "ce-src-123",
				CloudEventType:   "ce-type-123",
				Payload: func() json.RawMessage {
					ce := cloudevents.NewEvent()
					_ = ce.SetData(cloudevents.TextPlain, `
{
"actor": "actor123",
"emulator": "emulator123"
}`)
					return ce.Data()
				}(),
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.NoError(t, err)
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := wiretapDataToTracedMessage(tt.args.data)
			if !tt.wantErr(t, err, fmt.Sprintf("wiretapDataToTracedMessage(%v)", tt.args.data)) {
				return
			}
			assert.Equalf(t, tt.want, got, "wiretapDataToTracedMessage(%v)", tt.args.data)
		})
	}
}

func TestLogFileName(t *testing.T) {
	type args struct {
		logDir         string
		fileNamePrefix string
		currTime       time.Time
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "empty dir string",
			args: args{
				logDir:         "",
				fileNamePrefix: "",
				currTime:       time.Date(2021, 1, 29, 16, 58, 57, 0, time.UTC),
			},
			want: "2021-01-29.log",
		},
		{
			name: "current dir & no prefix",
			args: args{
				logDir:         "./",
				fileNamePrefix: "",
				currTime:       time.Date(2022, 12, 22, 1, 2, 3, 4, time.UTC),
			},
			want: "2022-12-22.log",
		},
		{
			name: "current dir & prefix",
			args: args{
				logDir:         "",
				fileNamePrefix: "foobar",
				currTime:       time.Date(2022, 12, 22, 1, 2, 3, 4, time.UTC),
			},
			want: "foobar2022-12-22.log",
		},
		{
			name: "parent dir & prefix",
			args: args{
				logDir:         "../",
				fileNamePrefix: "foobar",
				currTime:       time.Date(2022, 12, 22, 1, 2, 3, 4, time.UTC),
			},
			want: "../foobar2022-12-22.log",
		},
		{
			name: "absolute path & prefix",
			args: args{
				logDir:         "/etc/foo/bar",
				fileNamePrefix: "baz-",
				currTime:       time.Date(2022, 12, 22, 1, 2, 3, 4, time.UTC),
			},
			want: "/etc/foo/bar/baz-2022-12-22.log",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, LogFileName(tt.args.logDir, tt.args.fileNamePrefix, tt.args.currTime))
			assert.Equal(t, logFileName(tt.args.logDir, tt.args.fileNamePrefix, dateString(tt.args.currTime)), LogFileName(tt.args.logDir, tt.args.fileNamePrefix, tt.args.currTime))
		})
	}
}
