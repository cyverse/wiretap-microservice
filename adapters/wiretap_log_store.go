package adapters

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

// WiretapLogStore is an empty structure
type WiretapLogStore struct {
}

// Init is empty initialization for WiretapLogStore
func (wfs *WiretapLogStore) Init(c utils.Config) {
	log.Debug("WiretapLogStore.Init started")
}

// Create outputs the message into
func (wfs *WiretapLogStore) Create(data types.WiretapData) error {
	log.Debug("WiretapL:ogStore.Create started")

	logEvent(log.StandardLogger(), data)

	b := data.Data.Data()
	if b == nil {
		log.Warn("WiretapLogStore.Create CloudEvent data could not be retrieved")
		// for now, don't print error, just print nil
	} else {

		// for now, let's convert b to a string; if b is a marshalled json text, the print should output as expected
		log.Printf("\tCloud Event (id=%s, byte length=%d): %s\n", data.Data.Context.GetID(), len(b), string(b))

	}

	return nil
}
