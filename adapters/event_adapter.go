package adapters

import (
	"context"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"
	"sync"
	"time"

	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

// StanEventAdapter is an example of a Driver Adapter. Specifically, this is supposed to be a source of information and
// needs to be able to call functions in the Domain.
type StanEventAdapter struct {
	config            utils.Config
	domainChannel     chan<- types.WiretapData
	wg                *sync.WaitGroup
	internalWaitGroup *sync.WaitGroup
	ctx               context.Context
	nc                *nats.Conn
	sc                stan.Conn
}

// Init is the init function required per the Port interface
func (e *StanEventAdapter) Init(c utils.Config) {
	e.config = c
	log.WithFields(log.Fields{
		"function":  "adapters.StanEventAdapter.Init",
		"natsURL":   e.config.NatsURL,
		"clusterID": e.config.NatsClusterID,
	}).Info("config set")
}

// Start connects to NATS Streaming and subscribe to events, events will be sent to domain channel.
// One can use the cancel function for the context parameter to stop incoming events.
// WaitGroup.Done() will be called on the WaitGroup parameter before this function returns to signal that this adapter
// has stopped (no more events will be written to domain channel).
func (e *StanEventAdapter) Start(ctx context.Context, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"function": "adapters.StanEventAdapter.Start",
	})
	logger.Debug("called")
	e.ctx = ctx
	e.wg = wg
	e.internalWaitGroup = &sync.WaitGroup{}
	defer e.wg.Done()

	e.stanConnect(logger)
	e.stanAsyncSubscribe(logger)

	<-ctx.Done()
	e.Close()

	// this will wait for all StanEventAdapter.msgHandler() that can write to domainChannel
	e.internalWaitGroup.Wait()
}

func (e *StanEventAdapter) stanConnect(logger *log.Entry) {
	nc, err := nats.Connect(e.config.NatsURL, nats.MaxReconnects(types.DefaultNatsMaxReconnect), nats.ReconnectWait(types.DefaultNatsReconnectWait))
	if err != nil {
		logger.WithError(err).WithFields(log.Fields{
			"natsURL":       e.config.NatsURL,
			"maxReconnect":  types.DefaultNatsMaxReconnect,
			"reconnectWait": types.DefaultNatsReconnectWait,
		}).Fatal("cannot connect to nats")
	}
	e.nc = nc
	logger.Debug("connected to nats")

	sc, err := stan.Connect(e.config.NatsClusterID, e.config.NatsClientID, stan.NatsConn(nc))
	if err != nil {
		logger.Fatal("cannot connect to nats streaming, ", e.config.NatsClusterID, ":", err)
	}
	e.sc = sc
	logger.WithFields(log.Fields{
		"clusterID": e.config.NatsClusterID,
		"clientID":  e.config.NatsClientID,
	}).Debug("connected to nats streaming")
}

func (e *StanEventAdapter) stanAsyncSubscribe(logger *log.Entry) {
	logger.WithField("subject", e.config.NatsEventsSubject).Debug("subscribing on subject")
	_, err := e.sc.QueueSubscribe(
		e.config.NatsEventsSubject,
		e.config.NatsQgroup,
		e.msgHandler,
		stan.DurableName(e.config.NatsDurableName),
		stan.SetManualAckMode(),
	)
	if err != nil {
		logger.Fatal("cannot create a queued subscription")
	}
}

func (e *StanEventAdapter) msgHandler(m *stan.Msg) {
	logger := log.WithFields(log.Fields{
		"function": "adapters.StanEventAdapter.msgHandler",
		"subject":  m.Subject,
		"reply":    m.Reply,
	})

	// use an internal WaitGroup to ensure that all msg handler launched before ctx is canceled gets recognized by
	// the e.internalWaitGroup.Wait() in StanEventAdapter.Start().
	// if e.internalWaitGroup.Add(1) happens BEFORE e.internalWaitGroup.Wait() then Wait() will wait for this go routine
	// to finish.
	// if e.internalWaitGroup.Add(1) happens AFTER e.internalWaitGroup.Wait() then context is already canceled, so this
	// go routine will simply return after checking on context with StanEventAdapter.ctxCancelled()
	e.internalWaitGroup.Add(1)
	defer e.internalWaitGroup.Done()

	logger.Trace("received message")
	if e.ctxCancelled() {
		logger.Info("context cancelled, msg ignored")
		return
	}

	wdata := types.WiretapData{}
	wdata.Subject = m.Subject
	wdata.Reply = m.Reply
	wdata.Sequence = m.Sequence
	wdata.Timestamp = m.Timestamp
	wdata.Data = utils.StanToCloudEvent(m)
	logger.Trace("m.data = " + string(m.Data))

	// write to shared domain channel
	logger.Trace("sending domain data")
	select {
	case e.domainChannel <- wdata:
		_ = m.Ack()
		logger.Trace("done sending domain data")
	case <-time.After(time.Millisecond * 300):
		// timeout if domain cannot process the msg in channel
		logger.Trace("no message sent, domain channel is blocked")
		break
	}
}

// InitChannel initialize the shared channel with the Domain object
func (e *StanEventAdapter) InitChannel(dc chan<- types.WiretapData) {
	e.domainChannel = dc
}

func (e *StanEventAdapter) ctxCancelled() bool {
	select {
	case <-e.ctx.Done():
		return true
	default:
		return false
	}
}

// Close closes NATS and NATS Streaming connection
func (e *StanEventAdapter) Close() {
	e.nc.Close()
	err := e.sc.Close()
	if err != nil {
		log.WithError(err).Error("fail to close STAN connection")
	}
}
