package adapters

import (
	"context"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/nats-io/nats.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/wiretap-microservice/ports"
	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
	"sync"
	"time"
)

const (
	natsJetStreamDurableName = "wiretap-service"
	natsJetStreamQueueName   = "wiretap-service"
)

// EventAdapterNATSJS is an implementation of ports.IncomingEventPort based on NATS JetStream
type EventAdapterNATSJS struct {
	nc      *nats.Conn
	jet     nats.JetStream
	channel chan *nats.Msg
	dc      chan<- types.WiretapData
}

var _ ports.IncomingEventPort = (*EventAdapterNATSJS)(nil)

// Init ...
func (e *EventAdapterNATSJS) Init(c utils.Config) {
	logger := log.WithFields(log.Fields{
		"function": "EventAdapterNATSJS.Init",
	})
	nc, err := natsConnect(logger, c)
	if err != nil {
		return
	}
	e.nc = nc
	jet, err := e.nc.JetStream()
	if err != nil {
		return
	}
	_, err = jet.AddConsumer(messaging2.CyVerseStreamName, &nats.ConsumerConfig{
		Durable:            natsJetStreamDurableName,
		Name:               "",
		Description:        "",
		DeliverPolicy:      0,
		OptStartSeq:        0,
		OptStartTime:       nil,
		AckPolicy:          0,
		AckWait:            time.Second * 3,
		MaxDeliver:         0,
		BackOff:            nil,
		FilterSubject:      "",
		ReplayPolicy:       0,
		RateLimit:          0,
		SampleFrequency:    "",
		MaxWaiting:         0,
		MaxAckPending:      10,
		FlowControl:        false,
		Heartbeat:          0,
		HeadersOnly:        false,
		MaxRequestBatch:    0,
		MaxRequestExpires:  0,
		MaxRequestMaxBytes: 0,
		DeliverSubject:     nats.NewInbox(),
		DeliverGroup:       natsJetStreamQueueName,
		InactiveThreshold:  0,
		Replicas:           0,
		MemoryStorage:      false,
	})
	if err != nil {
		return
	}
	e.jet = jet
}

func natsConnect(logger *log.Entry, config utils.Config) (*nats.Conn, error) {
	nc, err := nats.Connect(config.NatsURL, nats.MaxReconnects(types.DefaultNatsMaxReconnect), nats.ReconnectWait(types.DefaultNatsReconnectWait))
	if err != nil {
		logger.WithError(err).WithFields(log.Fields{
			"natsURL":       config.NatsURL,
			"maxReconnect":  types.DefaultNatsMaxReconnect,
			"reconnectWait": types.DefaultNatsReconnectWait,
		}).Error("cannot connect to nats")
		return nil, err
	}
	logger.Debug("connected to nats")
	return nc, nil
}

// Start ...
func (e *EventAdapterNATSJS) Start(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()
	c := make(chan *nats.Msg, 10)
	e.channel = c
	sub, err := e.jet.ChanQueueSubscribe(common.EventsSubject+".>", natsJetStreamQueueName, c, nats.Durable(natsJetStreamDurableName))
	if err != nil {
		log.WithError(err).Panic("fail to start NATS JetStream subscription")
		return
	}
	defer sub.Drain()
	e.receiveMessages(ctx, c)
}

func (e *EventAdapterNATSJS) receiveMessages(ctx context.Context, c <-chan *nats.Msg) {
	for {
		var msg *nats.Msg
		select {
		case msg = <-c:
		case <-ctx.Done():
			return
		}
		err := msg.AckSync()
		if err != nil {
			log.WithError(err).Error("fail to ack NATS JetStream msg")
			continue
		}

		metadata, err := msg.Metadata()
		if err != nil {
			log.WithError(err).Error("fail to get metadata from NATS JetStream msg")
			e.dc <- types.WiretapData{
				Subject: msg.Subject,
			}
			continue
		}
		ce, err := messaging2.ConvertNats(msg)
		if err != nil {
			log.WithError(err).Error("fail to convert NATS JetStream msg to cloudevent")
			e.dc <- types.WiretapData{
				Subject:   msg.Subject,
				Reply:     "",
				Data:      cloudevents.Event{},
				Event:     "",
				Sequence:  metadata.Sequence.Stream,
				Timestamp: metadata.Timestamp.Unix(),
			}
			continue
		}
		e.dc <- types.WiretapData{
			Subject:   msg.Subject,
			Reply:     "",
			Data:      ce,
			Event:     "",
			Sequence:  metadata.Sequence.Stream,
			Timestamp: metadata.Timestamp.Unix(),
		}
	}
}

// InitChannel ...
func (e *EventAdapterNATSJS) InitChannel(dc chan<- types.WiretapData) {
	e.dc = dc
}

// Close ...
func (e *EventAdapterNATSJS) Close() {
	e.nc.Close()
}
