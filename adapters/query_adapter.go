package adapters

import (
	"context"
	"strings"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/nats-io/nats.go"
	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

// QueryAdapter is an example of a Driver Adapter.
type QueryAdapter struct {
	config        utils.Config
	domainChannel chan<- types.WiretapData
	wg            *sync.WaitGroup
	ctx           context.Context
	nc            *nats.Conn
}

// Init is the init function required per the Port interface
func (q *QueryAdapter) Init(c utils.Config) {
	q.config = c
	log.WithFields(log.Fields{
		"function": "adapters.QueryAdapter.Init",
		"natsURL":  q.config.NatsURL,
	}).Info("config set")
}

// Start connects to NATS and subscribe to queries, queries are sent to domain channel.
// One can use the cancel function for the context parameter to stop incoming events.
// WaitGroup.Done() will be called on the WaitGroup parameter before this function returns to signal that this adapter
// has stopped (no more queries will be written to domain channel).
func (q *QueryAdapter) Start(ctx context.Context, wg *sync.WaitGroup) {
	logger := log.WithFields(log.Fields{
		"function": "adapters.QueryAdapter.Start",
	})
	logger.Debug("called")
	q.ctx = ctx
	q.wg = wg

	defer q.wg.Done()

	q.natsConnect(logger)
	q.syncSubscribe(ctx, logger)
}

func (q *QueryAdapter) natsConnect(logger *log.Entry) {
	nc, err := nats.Connect(q.config.NatsURL, nats.MaxReconnects(types.DefaultNatsMaxReconnect), nats.ReconnectWait(types.DefaultNatsReconnectWait))
	if err != nil {
		logger.WithError(err).WithFields(log.Fields{
			"natsURL":       q.config.NatsURL,
			"maxReconnect":  types.DefaultNatsMaxReconnect,
			"reconnectWait": types.DefaultNatsReconnectWait,
		}).Fatal("cannot connect to nats")
	}
	q.nc = nc
	logger.Debug("connected to nats")
}

func (q *QueryAdapter) syncSubscribe(ctx context.Context, logger *log.Entry) {
	logger.Debug("subscribing on subject '>'")

	sub, err := q.nc.QueueSubscribeSync(">", q.config.NatsQgroup)
	if err != nil {
		logger.WithError(err).Fatal("cannot create a queued sync subscription")
	}
	for {
		var msg *nats.Msg
		msg, err = sub.NextMsgWithContext(ctx)
		if err != nil {
			logger.WithError(err).Error("fail to get next nats msg")
			if q.ctxCancelled() {
				q.Close()
				return
			}
			continue
		}
		q.msgHandler(msg)
	}
}

func (q *QueryAdapter) msgHandler(m *nats.Msg) {
	logger := log.WithFields(log.Fields{
		"function": "adapters.QueryAdapter.msgHandler",
		"subject":  m.Subject,
		"reply":    m.Reply,
	})
	// ignore stan pings
	if strings.Index(m.Subject, "_STAN") == 0 {
		logger.Trace("stan pings detected, skipping")
		return
	}

	logger.Trace("received message")
	if q.ctxCancelled() {
		logger.Info("context cancelled, msg ignored")
		return
	}

	wdata := types.WiretapData{
		Subject:   m.Subject,
		Reply:     m.Reply,
		Data:      utils.NatsToCloudEvent(m),
		Event:     "",
		Sequence:  0,
		Timestamp: 0,
	}

	if wdata.Data.Data() == nil {
		logger.Trace("data was nil, skipping")
		return
	}

	// write to shared domain channel
	logger.Trace("sending domain data")
	select {
	case q.domainChannel <- wdata:
		logger.Trace("done sending domain data")
	case <-time.After(time.Millisecond * 300):
		// timeout if domain cannot process the msg in channel
		logger.Trace("no message sent, domain channel is blocked")
	}
}

func (q *QueryAdapter) ctxCancelled() bool {
	select {
	case <-q.ctx.Done():
		return true
	default:
		return false
	}
}

// InitChannel initialize the shared channel with the Domain object
func (q *QueryAdapter) InitChannel(dc chan<- types.WiretapData) {
	q.domainChannel = dc
}

// Close ...
func (q *QueryAdapter) Close() {
	log.Info("closing nats connection")
	q.nc.Close()
}
