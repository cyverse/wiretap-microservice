package ports

import (
	"context"
	"sync"

	"gitlab.com/cyverse/wiretap-microservice/types"
	"gitlab.com/cyverse/wiretap-microservice/utils"
)

// Port is the base interface for Ports, in which all should support an Init() function (but it may be noop)
type Port interface {
	Init(c utils.Config)
}

// AsyncPort is port that will have an asynchronous behavior to it. The adapter implement this port will run on a separate go routine.
// The adapter that implement this interface should be cancel-able via the context passed to Start().
// The adapter should also call WaitGroup.Done() on the wait group received via Start() when the go routine exits.
//
// Domain can use cancel function of the context to signal adapter to stop what it is doing and use wait group to be
// notified of when adapter has exited.
type AsyncPort interface {
	Port
	Start(ctx context.Context, wg *sync.WaitGroup)
}

// IncomingQueryPort is an interface for accepting incoming queries.
// The adapter that implement this port will send the queries it receives to the go channel it received via InitChannel().
type IncomingQueryPort interface {
	AsyncPort
	InitChannel(dc chan<- types.WiretapData)
	Close()
}

// IncomingEventPort is an interface for accepting incoming events.
// The adapter that implement this port will send the events it receives to the go channel it received via InitChannel().
type IncomingEventPort interface {
	AsyncPort
	InitChannel(dc chan<- types.WiretapData)
	Close()
}

// PersistentStoragePort is a port to store messages
type PersistentStoragePort interface {
	Port
	Create(d types.WiretapData) error
	Close()
}
