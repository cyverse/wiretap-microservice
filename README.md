# wiretap-microservice

This project contains the wiretap microservice, a simple listener to nats channels

## Integration tests
Test against a real instance of nats-streaming container. The test is set up to run as a part of gitlab CI.

### how to run locally

Use docker-compose to run the nats-streaming container
```shell
cd tests
docker-compose up -d
export CI_INTEGRATION_STAN=true
export NATS_URL=nats://localhost:4222
export NATS_CLUSTER_ID=test-cluster
go test -v .
```

cleanup container (still in `tests/` directory)
```shell
docker-compose down
```
